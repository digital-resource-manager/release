export default function useFilterMenu() {
	const inputFields = [
		{
			text: "Kontinent",
			id: continents
		},
		{
			text: "Land",
			id: countries
		},
		{
			text: "Bundesland",
			id: provincestate
		},
		{
			text: "Ort",
			id: cities
		},
		{
			text: "Objekt",
			id: objects
		}
	]
};
