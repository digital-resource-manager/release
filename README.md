[[_TOC_]]

## Digitaler Ressourcen Manager

Das Corpus Vitrearum Medii Aevi (CVMA) Deutschland ist ein interakademisches Forschungsvorhaben mit Arbeitsstellen in Freiburg (AdWL) und Potsdam (BBAW) sowie als Teil des Internationalen Corpus Vitrearum Participant der NFDI4Culture. Seine Aufgaben sind die Erfassung und Erschließung mittelalterlicher Glasmalereien in Deutschland. Nach wissenschaftlichen Grundsätzen erstellte fotografische Aufnahmen bilden die Basis der Dokumentations- und Erschließungsarbeit. Sie werden neben der Veröffentlichung in Corpusbänden als nachnutzbare Forschungsdaten auf der Website des CVMA Deutschland publiziert (http://corpusvitrearum.de) und mit Metadaten angereichert. 2015 wurde durch die Arbeitsstellen des CVMA, die Digitale Akademie Mainz und TELOTA - IT/DH der BBAW eine XMP-Spezifikation für die Annotation der Digitalisate verabschiedet (https://corpusvitrearum.de/cvma-digital/spezifikationen/cvma-xmp/11.html).

Der CVMA Digitaler Ressourcen Manager (DRM) wird seit 2021 als Arbeitsinstrument für die Erfassung, Modellierung und Recherche von wissenschaftlichen Bildbeständen entwickelt. Als Ergebnis des Testens existierender DAM sowie im Zuge der Diskussionen der Community wurde deutlich, dass im Bereich der wissenschaftlichen Verarbeitung von Bilddaten Desiderate bestehen. Das gilt besonders für die Thematik der Verarbeitung von XMP, aber auch darüber hinaus.Dazu gehören Defizite in der Anzeige und Durchsuchbarkeit von XMP-Metadaten ebenso wie die Anpassbarkeit an eigene Spezifikationen und die Möglichkeit, kontrollierte Vokabulare einzufügen. Ein weiteres Themenfeld ist die Verarbeitung großer Bilddateien. Viele Tools müssen deshalb lokal ausgeführt werden, was kollaboratives Arbeiten erschwert und permanente Datentransfers und komplexe Backupstrategien erfordert.

Einen Einblick in die Arbeitsweise des DRM beim Projekt CVMA kann man [hier](https://gitlab.com/digital-resource-manager/release/-/blob/main/CVMA_Beispiel.pdf) gewinnen.

CVMA und TELOTA entwickeln eine webbasierte Lösung, die sämtliche Komponenten von der Serververknüpfung über die Verwaltung und Visualisierung der Daten bis hin zur für die Benutzer:innen sichtbaren Bearbeitungsoberfläche und Suchfunktion vereint. Schon in der jetzigen Entwicklungsphase zeigt sich, dass eine in sich konsistente Gesamtlösung für die Softwarekomponenten sehr effizient ist. Die Bilddaten sind zentral abgelegt. Die zugehörigen Metadaten sind sowohl im Bild als auch in JSON-Filialdateien als menschen- und maschinenlesbare Dateien synchronisiert gespeichert. Die JSON-Dateien ermöglichen einen effizienten und ressourcenschonenden Zugriff auf die Metadaten. Auf die Bedürfnisse des CVMA hin wurden zudem verschiedene Funktionalitäten der wissenschaftlichen Bildverwaltung implementiert (Details s. u.). Der DRM ist jedoch anpassbar auf andere Dateiformate, z.B. auch 3D-Daten.

Die Konfiguration des DRM wird niedrigschwellig über extern einzulesende Dateien gestaltet. Anpassungen der Anzeige entsprechend eigener Bedürfnisse, das Erstellen von Auswahllisten und eine Einbindung von kontrolliertem Vokabular lassen sich so realisieren. Durch Veränderung der Serververknüpfungen kann der DRM auf jedem Server laufen.

Da sowohl der komplette Quellcode als auch die verwendeten Bibliotheken frei verfügbar sind, können diese an die Anforderungen Dritter angepasst werden. Der Quellcode wird auch in Git-Repositorien publiziert und ermöglicht so eine gemeinsame Weiterentwicklung. Bisher existiert eine Entwickleroberfläche. 

## Adressaten für die Nutzung des DRM
Der DRM ist vorderrangig für ein Szenario konzipiert bei dem es notwendig ist auf die Objekte (in der Regel Bilder) direkt zuzugreifen um Informationen (XMP etc.) auszulesen.
Diese Daten werden dann in der Oberfläche verarbeitet und können wieder ins Bild oder in andere Verzeichnisse geschrieben werden. 
Daher ist das Tool u.a. für Archive oder Museen geignet, die eventuell noch große Altbestände verarbeiten müssen. Der DRM kann an die jeweilige Serverstruktur angepasst werden, so dass keine Verschiebung von Daten notwendig ist (idealerweise handelt es sich dabei um Linux-basierte Systeme). 
Ein Feature ist die tiefergehende Auswertung der Bildobjekte, da es nicht selten vorkommt, dass diese Metadaten-Container korrumpiert sind. Dafür wird das Bild einem Bereinigungsmechanismus unterworfen. Diese Skripte können/müssen an die jeweilige Umgebung angepasst werden. 
Für Fragen könnt ihr uns jederzeit kontaktieren (siehe unten).

## Erläuterung der Konsolen-Skripte und Dateien
Die Anleitung bezieht sich auf die typische Serverstruktur 

### Konfiguration des DRM
in public/config gibt es die Datei drm_config.json in der die Einstellungen des DRM konfiguriert werden. Das betrifft die 
Tabellenspalten (Bezeichnung, XMP-Key) und individuelle Anweisungen für die Verarbeitung der Daten.

### Abfrage der Ordner und auslesen der Bilder (bash)
Der DRM kann die Ordner auf die er Zugriff hat überprüfen und bei Bedarf Bilder anmelden in dem er die Metadaten erstellt und zu jedem Objekt eine Metadatendatei 
<name>.meta erstellt sowie für jeden Ordner eine *.json Datei, welche für die Darstellung im DRM notwendig ist. 


### Schreiben der Metadaten in das Bild (php) 
Um mit dem Server zu kommunizieren muss in dieser Konfiguration php installiert und ein Script fetch.php ausgeführt werden. 


## Installation der Entwicklungsumgebung

### Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

#### Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

#### Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

#### Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
## Mitmachen
Dieses Projekt wird noch weiter entwickelt und stetig angepasst.
Bei Fragen und Anregungen gerne an Gordon Fischer (gfischer@bbaw.de) oder Anja Gerber (anja.gerber@klassik-stiftung.de) schreiben.

## Lizenzen, Programmcode und Dokumentation

„Gefördert durch die Deutsche Forschungsgemeinschaft (DFG) im Rahmen der Nationalen Forschungsdateninfrastruktur – 441958017"
„funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under the National Research Data Infrastructure – 441958017".


© Berlin-Brandenburgische Akademie der Wissenschaften 2024

    Programmcode: LGPL 3.0
    Dokumentation: CC BY 4.0