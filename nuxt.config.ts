// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {enabled: true},
  nitro: {
    prerender: {
      routes: ['/public', '/data']
    }
  },
  router: {
    base: '/cvma/'
  },
  modules: [
    '@nuxt/ui',
    '@nuxt/image',
    '@pinia/nuxt'
  ],
  css: [
    "~/assets/css/main.css"
  ],
  ssr: false
})
