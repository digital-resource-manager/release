//Hier wird der Gesamtdatensatz (die Mutter aller JSON Files) geladen
//import datenbestand from "~/data/Datenbestand_CVMA.json"
let datenbestand = ref([])

const fetchDatenbestand = async () => {

	//datenbestand = await $fetch("https://telotawebdev.bbaw.de/cvma/data/Datenbestand_CVMA.json")
	//datenbestand = await $fetch("http://localhost:3000/data/Datenbestand_CVMA.json");
	datenbestand = await $fetch("/data/metadaten/Datenbestand_CVMA.json")
};
fetchDatenbestand();



export const useData = () => {

	return {
		datenbestand
	};
};


